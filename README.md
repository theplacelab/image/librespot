# Librespot + Songrec w/ Event Webhook

Docker image for [librespot](https://github.com/librespot-org/librespot/), a headless spotify client including track lookup and a webhook to POST client events to a URL of your choice. Presents a remote speaker on your network which sends the output to `LIBRESPOT_DEV`.

Also includes songrec which listens to what is playing over `SONGREC_DEV` and identifies it, sending identifications to the same webhook. This is intended to identify songs being played across the same alsa device that aren't from spotify (IE from an analog source or other streaming service without an API for song lookup.)

# Known Issue

- Cannot access snoop from inside docker container

## Librespot and Songrec binaries

Binaries for these are _not_ freshly compiled by this script. If you are seeing lots of errors from these, possibly need to upgrade them. Check the README in the `bin` folder.

## Run Docker Image

```
docker run -d \
--device /dev/snd \
--restart=unless-stopped \
-v *PATH TO ENV*:/usr/src/app/.env \
registry.gitlab.com/theplacelab/image/librespot


docker run -d \
-v "/var/run/pulse/native:/var/run/pulse/native" \
--restart=unless-stopped -v *PATH TO ENV*:/usr/src/app/.env \
registry.gitlab.com/theplacelab/image/librespot

```

## Configuration

```sh
PULSE_SERVER=unix:/var/run/pulse/native
DEVICE_NAME=Streambox
LIBRESPOT_DEV=hw:0,0
SONGREC_DEV="dsnoop:CARD=CODEC,DEV=0"
EVENTS_POST_TO="http://example.com/stream/event"
SPOTIFY_USERNAME=
SPOTIFY_PASSWORD=
SPOTIFY_CLIENT_ID=
SPOTIFY_CLIENT_SECRET=
```

| DEVICE_NAME           | Name of                                                                                                                          |
| --------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| DEVICE_NAME           | Librespot appears on network as this name                                                                                        |
| LIBRESPOT_DEV         | Alsa device to use as output (arecord -l to list)                                                                                |
| SONGREC_DEV           | Alsa device (typically snoop) that songrec should monitor                                                                        |
| SPOTIFY_USERNAME      | Spotify Premium Username (required)                                                                                              |
| SPOTIFY_PASS          | Spotify Premium Password (required)                                                                                              |
| EVENTS_POST_TO        | Webhook URL (optional). If provided, will POST events here as they happen. Leave blank if you don't care about events            |
| SPOTIFY_CLIENT_ID     | Client and Secret can be obtained from spotify developer and are used for track lookup. Leave blank if you don't care about this |
| SPOTIFY_CLIENT_SECRET | Client and Secret can be obtained from spotify developer and are used for track lookup. Leave blank if you don't care about this |

## Webhook

### Sample Event Payload

```json
{
  "source": "librespot",
  "event": "changed",
  "oldTrackId": "0zLcFLRTPMwwo2dULiV2k0",
  "trackId": "41LUI2mXScZDjIZWmvvWpR",
  "trackData": {
    "album": {
      "art": "https://i.scdn.co/image/ab67616d0000b273cc03676408b5f18e4526fd02",
      "year": "2005",
      "name": "The Sunset Tree ",
      "type": "album"
    },
    "song": { "name": "Up the Wolves" },
    "artist": { "name": "The Mountain Goats" }
  }
}
```
