# Songrec

## https://github.com/marin-m/SongRec

apt update

apt install -y curl software-properties-common  
apt-add-repository ppa:marin-m/songrec -y -u  
apt update  
apt install -y songrec build-essential libasound2-dev cargo pkg-config

# Librespot

git clone https://github.com/librespot-org/librespot.git  
cd librespot  
cargo install librespot --no-default-features --features alsa-backend  
mv ~/.cargo/bin/librespot ~/Code/dockerImage/librespot/bin
