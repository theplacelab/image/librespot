("use strict");
require("dotenv").config({ path: require("find-config")(".env") });
const proc = require("./modules/proc.js");
const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));

const sendEvent = async (body) => {
  try {
    const response = await fetch(process.env.EVENTS_POST_TO, {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body
    });
    const data = await response.json();
  } catch (e) {
    console.error("ERROR: OnEvent Webhook: recipient did not return OK");
  }
};

console.log(`SONGREC: Listening on ${process.env.SONGREC_DEV}`);
proc.start(
  "songrec",
  ["listen", "--json", "--audio-device", process.env.SONGREC_DEV],
  {},
  (data) => {
    try {
      const songrecData = JSON.parse(data.toString());
      const trackData = {
        album: {
          art: songrecData.track?.images?.coverarthq,
          year: songrecData.track?.sections[0]?.metadata.find(
            (item) => item.title === "Released"
          )?.text,
          name: songrecData.track?.sections[0]?.metadata.find(
            (item) => item.title === "Album"
          )?.text,
          type: songrecData.track?.type
        },
        song: {
          name: songrecData.track?.title
        },
        artist: {
          name: songrecData.track?.subtitle
        }
      };

      /* 
        Songrec doesn't give us a confidence rating, but since we are
        detecting an analog source (record player mostly) we can
        assume there will be SOME frequency skew. When matching
        on noise, songrec tends to return a skew of 0 (perfect match)
        so we reject these as suspicious.
      */
      if (songrecData.matches[0].frequencyskew === 0) {
        console.log(
          `REJECTING (skew is sus): ${trackData.song?.name}: ${JSON.stringify(
            songrecData.matches[0].frequencyskew
          )}`
        );
        sendEvent(
          JSON.stringify({
            source: "songrec",
            event: "trackinfo",
            trackId: "",
            trackData: {}
          })
        );
      } else {
        console.log(
          `MATCH: ${trackData.song?.name}: ${JSON.stringify(
            songrecData.matches[0].frequencyskew
          )}`
        );
        sendEvent(
          JSON.stringify({
            source: "songrec",
            event: "trackinfo",
            trackId: trackData.song?.name,
            trackData
          })
        );
      }
    } catch (e) {
      console.error(e);
    }
  }
);
