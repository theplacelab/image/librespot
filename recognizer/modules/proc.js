const fs = require("fs");
const kill = require("tree-kill");
const childProcess = require("child_process");

/*
(function () {
  var childProcess = require("child_process");
  var oldSpawn = childProcess.spawn;
  function mySpawn() {
    console.log("spawn called");
    console.log(arguments);
    var result = oldSpawn.apply(this, arguments);
    return result;
  }
  childProcess.spawn = mySpawn;
})();
*/
module.exports = {
  // Convert arguments to array
  list: {},
  argsArray: (commandFile, config) => {
    let args = fs.readFileSync(commandFile, "utf8");
    Object.keys(config).forEach((key) => {
      args = args.replace(`{${key}}`, config[key]);
    });
    Object.keys(process.env).forEach((key) => {
      args = args.replace(`{ENV.${key}}`, process.env[key]);
    });

    Object.keys(config).forEach((key) => {
      args = args.replace(`{${key}}`, config[key]);
    });
    args = args.replace(/(\r\n|\n|\r)/gm, "");
    args = args.replace(/\\/gm, "");
    args = args.trim();
    let arr = args.split(" ");
    arr.shift();
    return arr;
  },

  // Spawn a process
  start: (cmd, args, opts = {}, cb) => {
    if (module.exports.list[cmd]) {
      console.log(`Restarting ${cmd}...`);
      console.log(
        `...stopping ${cmd} (pid ${module.exports.list[cmd].pid})...`
      );
      kill(module.exports.list[cmd].pid);
    }
    console.log(`Starting ${cmd}...`);
    let output = "";

    const process = childProcess.spawn(cmd, args, opts);
    process.stdout.on("data", (data) => cb(data));
    process.stderr.on("data", (data) => (output += data.toString()));
    process.on("message", (msg) => console.log("Message from parent:", msg));
    process.on("error", (error) => console.log(`error: ${error.message}`));
    process.on("exit", (code, signal) => {
      if (code > 0) {
        console.log(
          `CRASH: ${cmd} exited with ` + `code ${code} and signal ${signal}`
        );
        console.log(output);
      }
    });
    process.stop = () => {
      console.log(`Stopping ${cmd} (pid ${process.pid})...`);
      kill(process.pid);
    };
    module.exports.list[cmd] = process;
    return process;
  }
};
