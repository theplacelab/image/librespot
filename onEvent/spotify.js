import fetch from "node-fetch";

const spotify = {
  getAuthToken: async () => {
    let credentials = Buffer.from(
      `${process.env.SPOTIFY_CLIENT_ID}:${process.env.SPOTIFY_CLIENT_SECRET}`
    ).toString("base64");

    var details = { grant_type: "client_credentials" };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    const response = await fetch(`https://accounts.spotify.com/api/token`, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Basic ${credentials}`
      },
      method: "POST",
      body: formBody
    });
    const data = await response.json();
    return data.access_token;
  },
  getTrackInfo: async (trackId) => {
    const authToken = await spotify.getAuthToken();
    const response = await fetch(
      `https://api.spotify.com/v1/tracks/${trackId}`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${authToken}`
        },
        method: "GET"
      }
    );
    const data = await response.json();
    return { ...data };
  }
};
export default spotify;
