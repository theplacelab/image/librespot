"use strict";
import "dotenv/config";
import fetch from "node-fetch";
import spotify from "./spotify.js";
const sendEvent = async (body) => {
  try {
    const response = await fetch(process.env.EVENTS_POST_TO, {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body
    });
    const data = await response.json();
  } catch (e) {
    console.error("ERROR: OnEvent Webhook: recipient did not return OK");
  }
};

const getTrackData = async () => {
  if (!process.env.TRACK_ID) return null;
  const trackInfo = await spotify.getTrackInfo(process.env.TRACK_ID);
  const trackData = {
    album: {
      art: trackInfo.album.images[0].url,
      year: trackInfo.album.release_date.split("-")[0],
      name: `${trackInfo.album.name} ${
        trackInfo.album.album_type === "single" ? "(single)" : ""
      }`,
      type: trackInfo.album.album_type
    },
    song: {
      name: trackInfo.name
    },
    artist: {
      name: trackInfo.artists.map((artist) => artist.name).join(" + ")
    }
  };
  return trackData;
};

if (process.env.EVENTS_POST_TO?.length > 0) {
  let trackData = await getTrackData();
  sendEvent(
    JSON.stringify({
      source: "librespot",
      event: process.env.PLAYER_EVENT,
      oldTrackId: process.env.OLD_TRACK_ID,
      trackId: process.env.TRACK_ID,
      trackDuration: process.env.DURATION_MS,
      trackPosition: process.env.POSITION_MS,
      trackData
    })
  );
}
