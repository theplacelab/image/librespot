#!/bin/sh
. /usr/src/app/.env

/usr/src/app/librespot \
--onevent "/root/.nvm/versions/node/v16.13.0/bin/node /usr/src/app/index.js" \
--device "$LIBRESPOT_DEV" \
--name "$DEVICE_NAME" \
--username "$SPOTIFY_USERNAME" \
--password "$SPOTIFY_PASSWORD" \
--backend alsa \
--initial-volume 100