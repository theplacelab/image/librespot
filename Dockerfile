FROM ubuntu

RUN apt-get update
RUN apt install -y \
    curl \
    libasound2 \
    libdbus-1-3 \
    libgtk-3-0 \
    libavahi-compat-libdnssd-dev \
    pulseaudio \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    alsa-utils \
    nano \
    && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NODE_VERSION=16.13.0
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} \
    && nvm use v${NODE_VERSION} \
    && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

WORKDIR /usr/src/app
COPY librespot /usr/src/app
COPY songrec /usr/src/app
COPY .env /usr/src/app/.env
COPY ./docker-entrypoint.sh /usr/src/app/docker-entrypoint.sh
COPY onEvent/ /usr/src/app/
COPY recognizer/ /usr/src/app/recognizer
RUN chmod +x /usr/src/app/docker-entrypoint.sh
RUN npm install -g yarn 
RUN cd /usr/src/app/; yarn 
RUN cd /usr/src/app/recognizer; yarn 

ENTRYPOINT ["/usr/src/app/docker-entrypoint.sh"]